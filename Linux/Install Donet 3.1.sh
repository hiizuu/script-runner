#!/bin/bash

wget https://packages.microsoft.com/config/ubuntu/`lsb_release -r | awk -F'\t' '{print$2}'`/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb

sudo apt update; \
sudo apt install -y apt-transport-https && \
sudo apt update && \
sudo apt install -y dotnet-sdk-3.1
rm packages-microsoft-prod.deb
