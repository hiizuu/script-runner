#!/bin/bash

sudo apt install python
sudo apt install git
sudo apt install node-fetch
sudo apt install ninja-build

cd ~
if [ ! -d "workspace" ]; then
    mkdir workspace
fi

cd ~/workspace

if [ -d "MixedReality-WebRTC-Android" ]; then
    mkdir MixedReality-WebRTC-Android
fi

git clone https://github.com/microsoft/MixedReality-WebRTC.git

cd MixedReality-WebRTC/tools/build/libwebrtc

./config.sh -d ~/workspace/MixedReality-WebRTC-Android -b branch-heads/71 -t android -c arm64
./checkout.sh
./build.sh -c Release