#!/bin/bash

sudo apt install python
sudo apt install git
sudo apt install node-fetch
sudo apt install ninja-build

cd ~
if [ ! -d "workspace" ]; then
    mkdir workspace
fi

cd ~/workspace

if [ -d "depot_tools" ]; then
    rm -rf depot_tools
fi

git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
export PATH=$PATH:~/workspace/depot_tools

if [ -d "webrtc_android" ]; then
    rm -rf webrtc_android
fi

mkdir webrtc_android
cd webrtc_android
fetch --nohooks webrtc_android
gclient sync
cd src/
./build/install-build-deps.sh

# Choose one of these
#########
# Not working
#git checkout branch-heads/m71
#git branch
#tools_webrtc/android/build_aar.py
#########
git checkout master
git branch
#########

# Choose one of these
#########
tools_webrtc/android/build_aar.py
#########
#gn gen out/Debug --args='target_os="android" target_cpu="arm"'
#gn gen out/Release --args='is_debug=false is_component_build=false rtc_include_tests=false target_os="android" target_cpu="arm"'
#ninja -C out/Debug
#ninja -C out/Release