#!/bin/bash

#VERSION
VERSION="1.0"

#COLOR INIT
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`

RED_B=`tput setab 1`
GREEN_B=`tput setab 2`
YELLOW_B=`tput setab 3`
BLUE_B=`tput setab 4`

RESET=`tput sgr0`

echo "Loaded Color"
