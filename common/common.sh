#!/bin/bash

#VERSION
VERSION="1.0"

source common/color.sh

function LOG()
{
	return `console.log(date +"%Y%m%d.%H%M%S%3N : " + $1);`
}

function pause()
{
	echo "${BLUE}Press any key and continue${RESET}"
	read -rsp "" -n1
}

function yesorno()
{
	echo "${YELLOW}"
	echo $'Continue?(Y/N)'
	echo "${RESET}"
	while true; do
    read -rsp "" -n1 yn
    	case $yn in
        	[Yy]* ) return 1;;
        	[Nn]* ) return 0;;
    	esac
	done
}

function checkError()
{
	if [ "$?" -ne "0" ]; then
		echo "${RED}"
  		echo $'\nProcess failed'
  	else
  		echo "${GREEN}"
  		echo $'\nProcess successful.'
	fi
    echo "${RESET}"
}

function fileCheck()
{
	if [ -e $1 ]; then
	return 1;
	fi
	return 0;
}

function check_process()
{
  [ ! "$1" ]  && return 0
  proc=`pgrep -f $1`;
  [ ! "$proc" ] && return 0 || return 1
}

function isNumber()
{
	re='^[0-9]+$'
	if ! [[ $1 =~ $re ]] ; then
 	  return 0;
	fi

	return 1
}

function fileCount()
{
    shopt -s nullglob
    numfiles=($1)
    return ${#numfiles[@]}
}

echo "Loaded Common Functions"

