#!/bin/bash

source common/common.sh

DIALOG_CANCEL=1
DIALOG_ESC=255

#### Initialize ####
if [[ $# -ne 0 ]]; then
    OSTYPE=$1
    DEAFAULT=0
else
    OSTYPE=$(uname)
    DEAFAULT=1
fi

VERSION="1.0"
BACKTITLE="Script Runner(${OSTYPE}). v${VERSION}"

function t(){ type "$1"&>/dev/null;}

DIA=dialog;
while :; do
      t dialog && DIA=dialog && break
      t whiptail && DIA=whiptail && break
      exec date +s"No dialog program found"
done;

declare -a sharedfunctions
declare -a functions
declare -a sufunctions

SCRIPTS=()

function loadFunctions()
{
    sharedfunctions=()
    functions=()
    sufunctions=()
    SCRIPTS=()

    if [[ $DEAFAULT -eq 1 ]]; then
        fileCount "shared/*.sh"
        if [ $? -ne 0 ]; then
            chmod 770 shared/*.sh
            for filename in "shared/*.sh"; do
                sharedfunctions=("${sharedfunctions[@]}" ${filename})
            done
        fi
    fi
    
    fileCount "${OSTYPE}/*.sh"
    if [ $? -ne 0 ]; then
        chmod 770 ${OSTYPE}/*.sh
        for filename in "${OSTYPE}/*.sh"; do
            functions=("${functions[@]}" ${filename})
        done
    fi
    
    fileCount "${OSTYPE}/su/*.sh"
    if [ $? -ne 0 ]; then
        chmod 770 ${OSTYPE}/su/*.sh
        for filename in "${OSTYPE}/su/*.sh"; do
            sufunctions=("${sufunctions[@]}" ${filename})
        done
    fi
    
    sharedcount=${#sharedfunctions[@]}
    count=${#functions[@]}
    sucount=${#sufunctions[@]}
    
    for ((i = 0; i < sharedcount; i++)); do
        basename="${sharedfunctions[$i]%.*}"
        functionname=${basename##*/}
        SCRIPTS=("${SCRIPTS[@]}" "$i")
        SCRIPTS=("${SCRIPTS[@]}" "Shared - $functionname")
    done

    for ((i = sharedcount; i < ((count+sharedcount)); i++)); do
        basename="${functions[$i-sharedcount]%.*}"
        functionname=${basename##*/}
        SCRIPTS=("${SCRIPTS[@]}" "$i")
        SCRIPTS=("${SCRIPTS[@]}" "${OSTYPE} - $functionname")
    done

    for ((i = ((count+sharedcount)); i < ((sucount+count+sharedcount)); i++)); do
         basename="${sufunctions[$i-count-sharedcount]%.*}"
         functionname=${basename##*/}
        SCRIPTS=("${SCRIPTS[@]}" "$i")
        SCRIPTS=("${SCRIPTS[@]}" "Admin - $functionname")
    done
}

function installPackages()
{
    dpkg -S `which dialog`
    if [ "$?" -ne "0" ]; then
        "${OSTYPE}/Install\ Packages.sh"
	fi
}

function yesno()
{
    $DIA --backtitle "${BACKTITLE}" --title "$1" --yesno "$2" 0 0
    # 0: Yes, 1: No, 255: Cancel 
}

function display() {
  $DIA --title "$1" --msgbox "$2" 0 0
}

function listup()
{
    exec 3>&1
    SELECTED=$($DIA --backtitle "${BACKTITLE}" \
    --title "Scripts" \
    --menu "Please select:" 0 0 0 "${SCRIPTS[@]}" 2>&1 1>&3)
    exit_status=$?
    exec 3>&-
    case $exit_status in
        $DIALOG_CANCEL)
            clear
            echo "Program terminated."
            exit
            ;;
        $DIALOG_ESC)
            clear
            echo "Program aborted." >&2
            exit 1
            ;;
    esac
}

function run()
{
    clear

    if [[ $SELECTED -ge $sharedcount+$count ]];then
        sudo "${sufunctions[$SELECTED-$sharedcount-$count]}"
    elif [[ $SELECTED -ge $sharedcount ]];then
        "${functions[$SELECTED-$sharedcount]}"
    else
        "${sharedfunctions[$SELECTED]}"
    fi

    checkError && pause
}

#### Main Menu Function ####
function main()
{
    listup
    run

    # yesno "Title" "Really?"
    # display "Disk Usage" "$(ls -la)"
    # pause
}

#### Main Function ####

clear
#installPackages
while true
	do loadFunctions && main && clear
done
