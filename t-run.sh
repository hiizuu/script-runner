#!/bin/bash

source common/common.sh

#### Initialize ####
if [[ $# -ne 0 ]]; then
    OSTYPE=$1
    DEAFAULT=0
else
    OSTYPE=$(uname)
    DEAFAULT=1
fi

VERSION="1.0"
BACKTITLE="Script Runner(${OSTYPE}). v${VERSION}"

SELECTED=0
declare -a sharedfunctions
declare -a functions
declare -a sufunctions

function loadFunctions()
{
    sharedfunctions=()
    functions=()
    sufunctions=()
    
    if [[ $DEAFAULT -eq 1 ]]; then
        fileCount "shared/*.sh"
        if [ $? -ne 0 ]; then
            chmod 770 shared/*.sh
            for filename in "shared/*.sh"; do
                sharedfunctions=("${sharedfunctions[@]}" ${filename})
            done
        fi
    fi
    
    fileCount "${OSTYPE}/*.sh"
    if [ $? -ne 0 ]; then
        chmod 770 ${OSTYPE}/*.sh
        for filename in "${OSTYPE}/*.sh"; do
            functions=("${functions[@]}" ${filename})
        done
    fi
    
    fileCount "${OSTYPE}/su/*.sh"
    if [ $? -ne 0 ]; then
        chmod 770 ${OSTYPE}/su/*.sh
        for filename in "${OSTYPE}/su/*.sh"; do
            sufunctions=("${sufunctions[@]}" ${filename})
        done
    fi
    
    sharedcount=${#sharedfunctions[@]}
    count=${#functions[@]}
    sucount=${#sufunctions[@]}
}

#### Main Menu Function ####
function main()
{
	clear
	echo "${YELLOW}Script Runner(${RED}${OSTYPE}${YELLOW}). v${VERSION}${RESET}"
	echo
    
    for ((i = 0; i < sharedcount; i++)); do
        basename="${sharedfunctions[$i]%.*}"
        functionname=${basename##*/}

        if [ $i -eq $SELECTED ]; then
            echo "${GREEN_B}${functionname}${RESET}"
        else
            echo "${functionname}"
        fi
    done

	for ((i = sharedcount; i < count+sharedcount; i++)); do
		basename="${functions[$i-sharedcount]%.*}"
		functionname=${basename##*/}

		if [ $i -eq $SELECTED ]; then
    		echo "${GREEN_B}${functionname}${RESET}"
		else
    		echo "${GREEN}${functionname}${RESET}"
		fi
	done

    for ((i = count+sharedcount; i < sucount+count+sharedcount; i++)); do
         basename="${sufunctions[$i-count-sharedcount]%.*}"
         functionname=${basename##*/}

         if [ $i -eq $SELECTED ]; then
             echo "${GREEN_B}${functionname}${RESET}"
         else
             echo "${RED}${functionname}${RESET}"
         fi
    done

	echo $'\n-------------------------------------\n'
	echo "${YELLOW}R. Reload Functions${RESET}"
	echo "${YELLOW}X. Exit${RESET}"
	echo

	echo "${GREEN}Select Function with the arrow keys and press enter${RESET}"
    read -rsp "" -n1 key

	case "$key" in
	"A")
		SELECTED=$((SELECTED-1 < 0 ? $count+$sucount+$sharedcount-1 : SELECTED-1))
		;;
	"B")
		SELECTED=$((SELECTED+1 > $count+$sucount+$sharedcount-1 ? 0 : SELECTED+1))
		;;
	"")
		clear
        if [[ $SELECTED -ge $sharedcount+$count ]];then
            sudo "${sufunctions[$SELECTED-$sharedcount-$count]}"
        elif [[ $SELECTED -ge $sharedcount ]];then
            "${functions[$SELECTED-$sharedcount]}"
        else
            "${sharedfunctions[$SELECTED]}"
        fi
		checkError && pause
		;;
	[Rr])
		loadFunctions
		echo "Reloaded all Functions"
		pause
		return 1;
		;;
	[Xx])
		return 0;
		;;
	esac

	return 1
}

#### Main Function ####

clear
loadFunctions
while ! main
	do clear
done
